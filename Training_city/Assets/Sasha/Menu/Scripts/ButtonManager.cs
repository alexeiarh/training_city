﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public List<Button> Buttons;

    public Color NormalColor;
    public Color SelectedColor;


    public void Start()
    {
        Buttons = gameObject.GetComponentsInChildren<Button>().ToList();

        foreach (var b in Buttons)
        {
            b.GetComponent<Image>().color = NormalColor;
        }
    }

    public void ButtonClick(Button butt)
    {
        foreach (var b in Buttons)
        {
            b.GetComponent<Image>().color = NormalColor;
        }

        butt.GetComponent<Image>().color = SelectedColor;
    }
}
