﻿using System.Collections;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Runtime;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;


public class Netmanager : MonoBehaviour
{
    public string host = "localhost";
    public int port = 7777;
    private bool socket_ready = false;
    
    TcpClient tcp_socket;
    NetworkStream net_stream;
    StreamWriter socket_writer;
    StreamReader socket_reader;
    // Start is called before the first frame update
    void Start()
    {
        setupSocket();
    }
    int temp = 1;
    void Update()
    {
       
        if (temp == 1)
        {
            string received_data = readSocket();
            if (received_data != null)
            {
                Debug.Log(received_data);
                writeSocket();
                temp += 1;
            }
        }

        

    }
    
    public void setupSocket()
    {
        try
        {
            tcp_socket = new TcpClient(host, port);
            net_stream = tcp_socket.GetStream();
            socket_writer = new StreamWriter(net_stream);
            socket_reader = new StreamReader(net_stream);
            socket_ready = true;
        }
        catch(System.Exception e)
        {
            Debug.Log("Socket error " + e);
        }
    }
    public void writeSocket()
    {
        if (!socket_ready)
        {
            return;
        }
        socket_writer.Write("OK");
        socket_writer.Flush();
    }
    public string readSocket()
    {
        if (!socket_ready)
        {
            return "Socket dead";
        }
        if (net_stream.DataAvailable)
        {
            //Debug.Log(Encoding.Default.GetString(socket_reader.Read()));
            return socket_reader.ReadLine();
        }
        return "";
    }
    public void closeSocket()
    {
        if (!socket_ready)
        {
            return;
        }
        socket_writer.Close();
        socket_reader.Close();
        tcp_socket.Close();
        socket_ready = false;
    }
}

