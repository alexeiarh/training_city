﻿using UnityEngine;
using PathCreation;
/// <summary>
/// Реализация передвижения автомобилей
/// </summary>
public class caring : MonoBehaviour
{
    public int postion;

    void Start()
    {
        rnd = new System.Random(System.Guid.NewGuid().GetHashCode());
        rend_col.material.SetColor("_Color", Random.ColorHSV());
    }

    public Renderer rend_col;
    private System.Random rnd;

    public GameObject Car;
    public PathCreator pathCreator;
    public float speed = 60;
    private float dist;
    private bool check = true;

    void Update()
    {

        if (postion > 0)
        {
            postion -= 1;
        }
        else
        {
            if (check)
            {
                dist += speed * Time.deltaTime;
                Car.transform.position = pathCreator.path.GetPointAtDistance(dist);
                Car.transform.rotation = pathCreator.path.GetRotationAtDistance(dist);
            }
        }
    }

    void OnTriggerEnter()
    {
        check = false;
    }
}