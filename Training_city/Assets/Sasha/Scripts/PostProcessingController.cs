﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
/// <summary>
/// При падении пользователя на землю включает PostProcessing 
/// </summary>
public class PostProcessingController : MonoBehaviour
{
    public float VignetteSpeed = 2.0f;
    public float AutoExposureSpeed = 0.5f;

    private PostProcessVolume _postProcessVolume;

    private Vignette _vignette;
    private AutoExposure _autoExposure;

    private float _t;
    private float _t2;

    public bool PlayerDied = false;

    void Start()
    {
        _postProcessVolume = GetComponent<PostProcessVolume>();

        _postProcessVolume.profile.TryGetSettings(out _vignette);
        _postProcessVolume.profile.TryGetSettings(out _autoExposure);
    }

    void Update()
    {
        if (PlayerDied) 
        { 
            Debug.Log("Player is Died!!");
            if(_vignette.intensity.value != 1)
            {
                _vignette.intensity.value = Mathf.Lerp(_vignette.intensity.value, 1, _t);
                _t += Time.deltaTime / VignetteSpeed;
            }
 
            if(_autoExposure.minLuminance.value != 9 && _autoExposure.maxLuminance.value != 9)
            {
                _autoExposure.minLuminance.value = Mathf.Lerp(_autoExposure.minLuminance.value, 9, _t2);
                _autoExposure.maxLuminance.value = Mathf.Lerp(_autoExposure.maxLuminance.value, 9, _t2);
                _t2 += Time.deltaTime / AutoExposureSpeed;
            }
        }
    }
}
