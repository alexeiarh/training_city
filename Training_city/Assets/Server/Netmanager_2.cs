﻿using System.Collections;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using System.Net;

public class Netmanager_2 : MonoBehaviour
{
    private Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    private byte[] recieveBuffer = new byte[800];//100 байт
    public int port = 7777;
    public string SEND_MESS = "OK";
    public string RECEIVED_MESS;
    public float Timer = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        SetupServer();
    }

    // Update is called once per frame
    void Update()
    {
        if(clientSocket.Connected == false)
        {
            Timer += Time.deltaTime;
        }
        if(clientSocket.Connected == false && Timer>3.0f)
        {
            Timer = 0;
            clientSocket.Close();
            clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SetupServer();

        }
    }
    private void OnApplicationQuit()
    {
        clientSocket.Close();
    }
    private void SetupServer()
    {
        try
        {
            clientSocket.Connect(new IPEndPoint(IPAddress.Loopback, port));
            Debug.Log("Connecting");
        }
        catch(SocketException e)
        {
            Debug.Log(e.Message);
            
        }
        SendData(Encoding.Default.GetBytes("OK"));
        clientSocket.BeginReceive(recieveBuffer, 0, recieveBuffer.Length, SocketFlags.None, new System.AsyncCallback(RecievedCallback), null);
        
    }
    private void RecievedCallback(System.IAsyncResult AR)
    {
        
       
        int recieved = clientSocket.EndReceive(AR);
        if(recieved <= 0)
        {
            return;
        }
        byte[] rec_data = new byte[recieved];
        System.Buffer.BlockCopy(recieveBuffer, 0, rec_data, 0, recieved);

        RECEIVED_MESS = Encoding.Default.GetString(recieveBuffer);
        //Debug.Log(Encoding.Default.GetString(recieveBuffer));
        SendData(Encoding.Default.GetBytes(SEND_MESS));
        clientSocket.BeginReceive(recieveBuffer, 0, recieveBuffer.Length, SocketFlags.None, new System.AsyncCallback(RecievedCallback), null);
    }
    private void SendData(byte[] data)
    {
        SocketAsyncEventArgs socketAsyncData = new SocketAsyncEventArgs();
        socketAsyncData.SetBuffer(data, 0, data.Length);
        clientSocket.SendAsync(socketAsyncData);
    }
}
