﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLeverRotation : MonoBehaviour
{
    private bool _rotated = false;
    public bool Rotated 
    { 
        get { return _rotated; }
        set { _rotated = value; }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Lever"))
        {
            Debug.Log("Collided!");
            _rotated = true;
        }
    }
}
