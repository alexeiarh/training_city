﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MySceneManager : MonoBehaviour
{
    public int CurrentSceneNum;

    public Pointer Pointer;
    public GameObject LoadingScreen;

    

    public Slider Slider;

    public void SetScene( int number)
    {
        CurrentSceneNum = number;
    }

    public void Submit()
    {
        //SceneManager.LoadScene(CurrentSceneNum);

        if (CurrentSceneNum != 0)
        {
            Pointer.enable = false;
            LoadingScreen.SetActive(true);
            StartCoroutine(LoadSceneAsync(CurrentSceneNum));

            if (LoadingScreen == null)
            {
                LoadingScreen = GameObject.Find("LoadingScreenCanvas");
            }
        }
    }

    IEnumerator LoadSceneAsync(int sceneNum)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneNum);

        asyncLoad.allowSceneActivation = false;

        while (!asyncLoad.isDone)
        {
            Slider.value = asyncLoad.progress;

            if (!asyncLoad.allowSceneActivation && asyncLoad.progress >= .9f)
            {
                asyncLoad.allowSceneActivation = true;
            }

            yield return null;
        }

        LoadingScreen.SetActive(false);
        Pointer.enable = true;
    }
}
