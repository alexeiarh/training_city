﻿using UnityEngine;
/// <summary>
/// Отслеживание падения пользователя на землю
/// </summary>
public class PlayerFell : MonoBehaviour
{
    public bool PlayerOnTheGround = false;

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Sphere")
        {
            Debug.Log("PlayerOnTheGround!");
            PlayerOnTheGround = true;
        }
    }
}
