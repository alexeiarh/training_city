﻿using UnityEngine;
/// <summary>
/// Ослеживание положения пользователя в пространстве и задаюние его поведения
/// </summary>
public class FallManager : MonoBehaviour
{
    public PlayerJumped PlayerJumped;
    public PlayerFell PlayerFell;
    public PlayerController PlayerController;
   // public PostProcessingController PostProcessingController;

    void Update()
    {
        ManageFalling();
    }

    private void ManageFalling()
    {
        if (PlayerJumped.PlayerInFreeFall)
        {
            PlayerController.PlayerIsFallingDown = true;

            if (PlayerFell.PlayerOnTheGround)
            {
                // это значит, что игрок приземлился на землю
                // PostProcessingController.PlayerDied = true;
            }
        }
        else
        {
            PlayerController.PlayerIsFallingDown = false;
        }
    }
}
