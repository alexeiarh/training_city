﻿using UnityEngine;
/// <summary>
/// Отслеживает, прыгнул ли пользователь
/// </summary>
public class PlayerJumped : MonoBehaviour
{
    [HideInInspector]
    public bool PlayerInFreeFall = false;

    public void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name == "Sphere")
        {
            //Debug.Log("PlayerInFreeFall!");
            PlayerInFreeFall = true;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Sphere")
        {
            PlayerInFreeFall = false;
        }
    } 
}
