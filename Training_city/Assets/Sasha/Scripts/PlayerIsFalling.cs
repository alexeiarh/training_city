﻿using UnityEngine;
/// <summary>
/// Скрипт падения.
/// </summary>
public class PlayerIsFalling : MonoBehaviour
{
    public GameObject Player;
    public GameObject Sphere;
    public float Delta;

    void Start()
    {
        Sphere.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, Player.transform.position.z);
        Delta = gameObject.transform.position.y - Sphere.transform.position.y;
    }

    void Update()
    {
        Player.transform.position = new Vector3(Player.transform.position.x, Sphere.transform.position.y, Player.transform.position.z);
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        Sphere.transform.position = new Vector3(gameObject.transform.position.x, Sphere.transform.position.y, gameObject.transform.position.z);
    }
}
