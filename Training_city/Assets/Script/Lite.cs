﻿using UnityEngine;

public class Lite : MonoBehaviour
{
    Light lite;
    float rng;
    public CheckLeverRotation pos;
    void Start()
    {
        lite = GetComponent<Light>();
        rng = lite.range;
    }

    void Update()
    {
        if(lite.range > 3)
        {
            if (pos.Rotated == true)
            {
                rng -= 0.0025f;
                lite.range = rng;
            }
        }
    }
}
