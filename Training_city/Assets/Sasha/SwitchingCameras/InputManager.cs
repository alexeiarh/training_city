﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class InputManager : MonoBehaviour
{
    [HideInInspector] public bool IsInVR;

    void Awake()
    {
        Debug.Log("XR Device Present: " + XRDevice.isPresent);
        Debug.Log("XR User Presence: " + XRDevice.userPresence);
        Debug.Log("XR Enabled: " + XRSettings.enabled);
        Debug.Log("XR Model: " + XRDevice.model);
        Debug.Log("XR Device Active: " + XRSettings.isDeviceActive);
    }

    void Update()
    {
        CheckUserPresence();
    }

    // Проверяем, надел ли пользователь шем. 
    // Если надел - управление переключается на VR (в MenuBehaviour в сцене меню), 
    // нет - управление кнопками (пока нет кнопок в DebagMode)
    private void CheckUserPresence()
    {
        if (XRDevice.userPresence == UserPresenceState.Present)
        {
            Debug.Log("XRDevice.userPresence Present");
            IsInVR = true;
        }
        else
        {
            Debug.Log("XRDevice.userPresence NOT Present");
            IsInVR = false;
        }
    }
}
