﻿using UnityEngine;
/// <summary>
/// Открытие дверей лифта
/// </summary>
public class lift: MonoBehaviour
{
    public GameObject door_left;
    public GameObject door_righ;
    public float timer = 0.0f;
    public AudioSource move_audio;
    public AudioSource door_audio;
    public AudioSource floor_audio;

    private bool audio_door_on = true;

    void Start()
    {
        door_audio.Stop();
        floor_audio.Stop();
    }

    void Update()
    {
        if (timer < 15.0f)
        {
            timer += Time.deltaTime;
        }
        else
        {
            move_audio.Stop();
            if (door_left.transform.localPosition.z > -0.6f)
            {
                if (audio_door_on)
                {
                    door_audio.Play();
                    floor_audio.Play();
                    audio_door_on = false;
                }
                door_left.transform.localPosition += Vector3.back * Time.deltaTime * 0.2f;
                door_righ.transform.localPosition -= Vector3.back * Time.deltaTime * 0.2f;
            }
            else
            {
                if (!audio_door_on)
                {
                    door_audio.Stop();
                    audio_door_on = true;
                }
            }
        }
    }
}
