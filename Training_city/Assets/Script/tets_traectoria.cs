﻿using System.Collections;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class tets_traectoria : MonoBehaviour
{
    public int postion;
    // Start is called before the first frame update
    void Start()
    {

    }

    public GameObject Car_test;
    public PathCreator traec;
    public float speed_test = 8;
    private float dist_test;
    // Update is called once per frame
    void Update()
    {

        dist_test += speed_test * Time.deltaTime;
        Car_test.transform.position = traec.path.GetPointAtDistance(dist_test);
        Car_test.transform.rotation = traec.path.GetRotationAtDistance(dist_test);
        

    }
}