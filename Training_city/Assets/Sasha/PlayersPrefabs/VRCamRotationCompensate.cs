﻿using UnityEngine;

public class VRCamRotationCompensate : MonoBehaviour
{
    public Transform DesiredHeadPositionTransform;
    public Transform CameraParentTransform;
    public Transform CameraTransform;

    private Vector3 deltaPos;
    private Vector3 tmp;

    public void Awake()
    {
    }

    public void Start()
    {
        if (DesiredHeadPositionTransform != null)
        {
            ResetSeatedPos(DesiredHeadPositionTransform);
            ResetY(DesiredHeadPositionTransform);
        }
        else
            Debug.Log("Необходимо вставить желаемый Transform в DesiredHeadPositionTransform!");
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (DesiredHeadPositionTransform != null)
                ResetSeatedPos(DesiredHeadPositionTransform);
            else 
                Debug.Log("Необходимо вставить желаемый Transform в DesiredHeadPositionTransform!");
        }
    }

    private void ResetY(Transform desiredHeadPos)
    {
        float cameraY = CameraTransform.transform.position.y;
        float desiredCameraY = desiredHeadPos.position.y;
        cameraY = cameraY + (desiredCameraY - cameraY);
        CameraParentTransform.position = new Vector3(CameraParentTransform.position.x, cameraY, CameraParentTransform.position.z);
    }
    
    private void ResetSeatedPos(Transform desiredHeadPos)
    {
        if (CameraParentTransform != null && CameraTransform != null)
        {
            // получение текущего разворота головы по Y
            float offsetAngle = CameraTransform.rotation.eulerAngles.y;
            // повернуть родительский объект камеры на заданное смещение головы (в этом месте пользователь разворачивается
            // в сторону оси Z, поэтому нужно доповернуть голову еще на "-100" градусов )
            CameraParentTransform.transform.Rotate(0f, -offsetAngle, 0f);
            
            // т.к. из-за поворота образовалось смещение между позициями камеры и ее родительского объекта,
            // то нужно вернуть родительский объект на место
            Vector3 offsetPos = CameraTransform.position - CameraParentTransform.position;
            CameraParentTransform.position = (desiredHeadPos.position - offsetPos);
            // Поставить корневой объект Player на то же место, что и камера
           // gameObject.transform.position = CameraParentTransform.position;
        }
        else
        {
            Debug.Log("Необходимо вставить пустой родительский объект камеры (в CameraParentTransform) и камеру (в CameraTransform)");
        }
    }
}
