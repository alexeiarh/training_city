﻿using UnityEngine;


public class WallMovement : MonoBehaviour
{
    private bool check = false;
    [SerializeField] private float movie = 0.0003f;
    public CheckLeverRotation CheckLeverRotation;
    public AudioSource audio;

    public void OnCollisionEnter(Collision collision)
    {
        audio.enabled = false;
        if(collision.gameObject.name == "Player")
        {
            // остановаиться, когда стена коснется человека
            check = false;
        }
    }

    void Update()
    {  
        if (check)
        {
            audio.enabled = true;
            gameObject.transform.Translate(new Vector3(gameObject.transform.position.z * movie, 0, 0));
            gameObject.transform.Translate(new Vector3(-gameObject.transform.position.x * movie, 0, 0));
        }
        if (Mathf.Abs(gameObject.transform.position.z) < 1 && Mathf.Abs(gameObject.transform.position.x) < 1)
        {
            check = false;
        }
        else
        {
            check = CheckLeverRotation.Rotated;
        }
    }

 }
